# README #

Wrekart is a simple E-Commerce Website.

![](previewImages/index.JPG)
![](previewImages/login.JPG)
![](previewImages/signup.JPG)
![](previewImages/cart.JPG)
![](previewImages/success.JPG)
![](previewImages/index_mob.JPG)
![](previewImages/mob_index_menu.JPG)
![](previewImages/login_mob.JPG)
![](previewImages/cart_empty_mob.JPG)
![](previewImages/cart_mob.JPG)
![](previewImages/success_mob.JPG)
