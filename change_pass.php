<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Change Password</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/login.css">
	<link rel="icon" href="images/favicon.png" type="image/gif">
</head>
<body>
	<div class="page_container">
		<?php 
			session_start();
			include_once 'header.php'; 
		?>
		<div class="content_wrapper">
			<div class="signup_login">
				<div id="change_pass_head" class="active">Change Password</div>
			</div>
			<div class="content">
				<div id="login_form">
					<form action="change_pass_back.php" method="post">
						<p><input id="email" type="email" name="email" placeholder="Email Address" required/></p>
						<p><input id="old_password" type="password" name="old_password" placeholder="Old Password" required/></p>
						<p><input id="new_password" type="password" name="new_password" placeholder="New Password" required/></p>
						<p><button id="form_submit" type="submit">Submit</button></p>
					</form>
				</div>
			</div>
		</div>
	</div>
    <div class="clear_both"></div>
    <footer>
        <span>&copy Wrekart 2017</span>
        <a href="#">About</a>
    </footer>
	<script src="js/change_pass.js"></script>
	<script src="https://use.fontawesome.com/5bac47f725.js"></script>
</body>
</html>
