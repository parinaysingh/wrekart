<?php
	session_start();
	
	if(empty($_SESSION['userid'])){
		header('location: login.php');
		exit();
	}
	if(empty($_GET['pid'])){
		header('location: index.php');
		exit();
	}
	
	require_once 'config.php';
	
	$pid = $conn->real_escape_string($_GET['pid']);
	
	$stmt = $conn->prepare("SELECT id FROM cart WHERE  (user_id = ?) AND (prod_id = ?)");
	$stmt->bind_param("ii", $_SESSION['userid'], $pid);
	$stmt->execute();
	$stmt->store_result();
	$stmt->fetch();
	if($stmt->num_rows){
		header('location: cart.php');
		exit();
	}
	$stmt->close();
	$stmt = $conn->prepare("INSERT INTO cart (user_id, prod_id) values(?, ?)");
	$stmt->bind_param("ii", $_SESSION['userid'], $pid);
	$stmt->execute();
	if($stmt->affected_rows){
		header('location: cart.php');
		exit();
	}else{
		echo "Error";
	}