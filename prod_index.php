<?php
	if(empty($_GET['sid'])){
		exit();
	}
	require_once 'config.php';
	$sid  = $conn->real_escape_string($_GET['sid']);
	$stmt = $conn->prepare("SELECT *FROM product LIMIT ?, 3");
	$stmt->bind_param("i", $sid);
	$stmt->execute();
	$resultSet = $stmt->get_result();
	$result = $resultSet->fetch_all();
	
	echo(json_encode($result));