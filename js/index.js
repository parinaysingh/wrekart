var content = document.getElementById("content");
var product_template = content.firstElementChild;
var load_more = document.getElementById("load_more");
var load = load_more.querySelector("button");
var sid = 6;
var xhttp;
if (window.XMLHttpRequest) {
    xhttp = new XMLHttpRequest();
} else {
    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var arr = JSON.parse(this.responseText);
        for (i = 0; i < arr.length; i++) {
            var product = product_template.cloneNode(true);
            product.querySelector(".product_image img").setAttribute("src", "images/" + arr[i][1]);       //product_image
            product.querySelector(".product_info").innerText = arr[i][2];                           	  //product_name
            product.querySelector(".product_info.inverted").innerText = arr[i][4];                  	  //product_specs
            product.querySelector(".product_price").innerText = arr[i][3];                          	  //product_price
            product.querySelector(".product_sub a").setAttribute("href", "add_cart.php?pid=" + arr[i][0]); //product_id
            content.appendChild(product);
        }
        if (arr.length > 0) {
            load_more.className = "";
        } else {
            load_more.className = "hidden";
        }
    }
}

load.addEventListener("click", function () {
	load_more.className = "loading";
    xhttp.open("GET", "prod_index.php?sid=" + sid, true);
    xhttp.send();
    sid += 3;
});