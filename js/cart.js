var content_wrapper = document.getElementById("content_wrapper");
var remove = document.getElementsByClassName("remove_button");
var empty_cart = document.getElementById("empty_cart");
var total_price = document.getElementById("total_price");

function removeElements() {
    var rem = this.parentElement;
    var node = rem.parentElement;
    var price = rem.previousElementSibling;
    var pid = node.firstElementChild.innerText;
    var xhttp;
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    } else {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (xhttp.responseText == "success") {
                content_wrapper.removeChild(node);
                total_price.innerText = parseInt(total_price.innerText) - parseInt(price.innerText.split(" ")[1]);
                if (content_wrapper.childElementCount < 3) {
                    empty_cart.classList.remove("hidden");
                }
            }
        }
    };
    xhttp.open("GET", "remove_cart.php?pid=" + pid, true);
    xhttp.send();
}

for (var i = 0; i < remove.length; i++) {
    remove[i].addEventListener('click', removeElements, false);
}