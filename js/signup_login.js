var login_form_submit = document.getElementById("login_form_submit");
var signup_form_submit = document.getElementById("signup_form_submit");
var login_form = document.getElementById("login_form");
var signup_form = document.getElementById("signup_form");
var login = document.getElementById("login");
var signup = document.getElementById("signup");

signup.addEventListener("click", function () {
    login_form.className = "hidden";
    signup_form.className = "show";
    this.style.background = "#19A881";
    login.style.background = "#24323D";
    login.innerText = "Log In";
});
login.addEventListener("click", function () {
    signup_form.className = "hidden";
    login_form.className = "show";
    this.style.background = "#19A881";
    signup.style.background = "#24323D";
    signup.innerText = "Sign Up";
});

function xhttpHandler(param, path, response) {
    response.innerText = "Submitting..";
    var xhttp;
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    } else {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText == "success") {
                response.innerText = "Redirecting...";
                window.location = "index.php";
            } else {
                response.innerText = this.responseText;
            }
        }
    };
    xhttp.open("POST", path, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(param);
}

login_form_submit.addEventListener('click', function (e) {
    e.preventDefault();
    var email = document.getElementById("login_email").value;
    var password = document.getElementById("login_password").value;
    var param = "email=" + email + "&password=" + password;
    xhttpHandler(param, "login_back.php", login);
});

signup_form_submit.addEventListener('click', function (e) {
    e.preventDefault();
    var fname = document.getElementById("signup_fname").value;
    var lname = document.getElementById("signup_lname").value;
    var email = document.getElementById("signup_email").value;
    var password = document.getElementById("signup_password").value;
    var param = "fname=" + fname + "&lname=" + lname + "&email=" + email + "&password=" + password;
    xhttpHandler(param, "register_back.php", signup);
});
