var form_submit = document.getElementById("form_submit");
var change_pass_head = document.getElementById("change_pass_head");
form_submit.addEventListener('click', function (e) {
    e.preventDefault();
    change_pass_head.innerText = "Submitting..";
    var email = document.getElementById("email").value;
    var old_password = document.getElementById("old_password").value;
    var new_password = document.getElementById("new_password").value;
    var xhttp;
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    } else {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText == "success") {
                change_pass_head.innerText = "Password Changed";
                window.location = "index.php";
            } else {
                change_pass_head.innerText = this.responseText;
            }
        }
    };
    var params = "email=" + email + "&old_password=" + old_password + "&new_password=" + new_password;
    xhttp.open("POST", "change_pass_back.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(params);
});