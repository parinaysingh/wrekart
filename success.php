<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wrekart</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/succcess.css">
	<link rel="icon" href="images/favicon.png" type="image/gif">
</head>
<body>
    <?php 
		session_start();
		require_once 'config.php';
		if(empty($_SESSION['userid'])){
			header('location: login.php');
			exit();
		}
		$userid = $_SESSION['userid'];
		$query = $conn->query("DELETE FROM cart WHERE user_id = '$userid'");
		if(!($conn->affected_rows)){
			header('location: cart.php');
			exit();
		}
	?>
	<div id="header_wrapper">
		<header>
			<div id="logo">
				<a href="index.php"><span class="fa fa-paper-plane"> Wrekart</span></a>
			</div>
			<nav>
				<div class="nav_item"><a href="login.php"><?php if(empty($_SESSION['userid'])){ echo "Log In"; }else{ echo "Log Out"; }?> <i class="fa fa-user" style="font-size:1.3em;" aria-hidden="true"></i></a></div>
				<div class="nav_item"><a href="cart.php">Cart <i class="fa fa-shopping-cart" style="font-size:1.3em;" aria-hidden="true"></i></a></div>
			</nav>
		</header>
		<div id="content_wrapper">
			<button class="notice">Thank You For Your Purchase !</button>
			<div class="button_action">
				<a href="index.php"><button>Continue Shopping</button></a>
			</div>
		</div>
	</div>
    <footer>
        <span>© Wrekart 2017</span>
        <a href="#">About</a>
    </footer>
	<script src="https://use.fontawesome.com/5bac47f725.js"></script>
</body>
</html>
