<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cart</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/cart.css">
	<link rel="icon" href="images/favicon.png" type="image/gif">
</head>
<body>
	<div class="page_container">
	<?php 
		session_start();
		require_once 'config.php';
		require_once 'header.php';
		if(empty($_SESSION['userid'])){
			header('location: login.php');
			exit();
		}
	?>
		<div class="scroll_bar">
			<img id="scroll_image" src="images/scroll/cart.jpg" alt="slide_image">
		</div>

		<div id="content_wrapper">
		<?php 
			$userid = $_SESSION['userid'];	
			$query = $conn->query("SELECT * FROM product WHERE id IN (SELECT prod_id FROM cart WHERE user_id = '$userid')");
			$total = 0;
			if($query->num_rows){
				while($result = $query->fetch_assoc()){
					
		?>
			<div class="content">
				<div class="hidden"><?php echo $result['id']; ?></div>
				<div class="product_img"><img src="images/<?php echo $result['image']; ?>"/></div>
				<div class="product_name"><?php echo $result['name']; ?></div>
				<div class="product_price">Rs <?php echo $result['price']; $total += $result['price']; ?> </div>
				<div class="remove"><button class="remove_button"><i class="fa fa-minus"></i></button></div>
			</div>
			
		<?php
				}
			}
		?>	
			<div id="empty_cart" class="content empty_notice <?php if($query->num_rows){ echo "hidden"; } ?>" >
				<div class="product_img "><img src="images/empty.gif"/></div>
			</div>
			
			<div class=" content button_action">
				<a href="index.php"><button class="continue_shopping">Continue Shopping</button></a>
				<button> Rs <span id="total_price"><?php echo $total; ?></span></button>
				<a href="success.php"><button class="proceed_checkout">Proceed to Checkout</button></a>
			</div>
		</div>
	</div>	
		<footer>
			<span>© Wrekart 2017</span>
			<a href="#">About</a>
		</footer>
    <script src="js/cart.js"></script>
	<script src="https://use.fontawesome.com/5bac47f725.js"></script>
</body>
</html>
