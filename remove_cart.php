<?php
	session_start();
	if(empty($_SESSION['userid'])){
		header('location: login.php');
		exit();
	}
	if(empty($_GET['pid'])){
		header('location: cart.php');
		exit();
	}
	require_once 'config.php';
	$pid = $conn->real_escape_string($_GET['pid']);
	
	$stmt = $conn->prepare("DELETE FROM cart WHERE (user_id = ?) and (prod_id = ?)");
	$stmt->bind_param("ii", $_SESSION['userid'], $pid);
	$stmt->execute();
	if($stmt->affected_rows){
		echo "success";
	}else{
		echo "error";
	}