<?php
	session_start();
	
	if(empty($_POST['email']) || empty($_POST['password'])){
		echo "Empty Fields";
		exit();
	}
	
	require_once 'config.php';
	
	$password = $conn->real_escape_string($_POST['password']);
	$email = $conn->real_escape_string($_POST['email']);
	
	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		echo "Invalid Email";
		exit();
	}
	
	$stmt = $conn->prepare("SELECT id, password from users WHERE email = ?");
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id, $pass);
	$stmt->fetch();
	if($stmt->num_rows){
		if(password_verify($password, $pass)){
			$_SESSION['userid'] = $id;
			echo "success";
			exit();
		}
	}
	echo "Login Failed";