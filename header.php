		<header>
			<div id="logo">
				<a href="index.php"><span class="fa fa-paper-plane"> Wrekart</span></a>
			</div>
			<nav>
				<div class="nav_item"><a href="login.php"><?php if(empty($_SESSION['userid'])){ echo "Log In"; }else{ echo "Log Out"; }?> <i class="fa fa-user" style="font-size:1.3em;" aria-hidden="true"></i></a></div>
				<div class="nav_item"><a href="cart.php">Cart <i class="fa fa-shopping-cart" style="font-size:1.3em;" aria-hidden="true"></i></a></div>
			</nav>
		</header>