<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wrekart</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/login.css">
	<link rel="icon" href="images/favicon.png" type="image/gif">
</head>
<body>
	<div class="page_container">
		<?php 
			session_start();
			session_unset();
			session_destroy();
			include_once 'header.php';
		?>

		<div class="content_wrapper">
			<div class="signup_login">
				<div id="login" class="active">Log In</div><div id="signup" class="">Sign Up</div>
			</div>
			<div class="content">
				<div id="login_form" class="">
					<form action="login_back.php" method="post">
						<p><input id="login_email" type="email" name="email" placeholder="Email Address" required/></p>
						<p><input id="login_password" type="password" name="password" placeholder="Password" required/></p>
						<p><button id="login_form_submit" type="submit">Log In</button></p>
						<p class="change_pass"><a href="change_pass.php">Change Password?</a></p>
					</form>
				</div>
				<div id="signup_form" class="hidden">
					<form action="register_back.php" method="post">
						<p class="signup_form_name">
							<input id="signup_fname" type="text" name="fname" placeholder="First Name" required/>
							<input id="signup_lname" type="text" name="lname" placeholder="Last Name" required/>
						</p>
						<p><input id="signup_email" type="email" name="email" placeholder="Email" required/></p>
						<p><input id="signup_password" type="password" name="password" placeholder="Password" required/></p>
						<p><button id="signup_form_submit" type="submit">Sign Up</button></p>
					</form>
				</div>
			</div>
		</div>
	</div>
	<footer>
        <span>© Wrekart 2017</span>
        <a href="#">About</a>
    </footer>
    <script src="js/signup_login.js"></script>
	<script src="https://use.fontawesome.com/5bac47f725.js"></script>
</body>
</html>
