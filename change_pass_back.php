<?php
	session_start();
	
	if(empty($_POST['email']) || empty($_POST['old_password']) || empty($_POST['new_password'])){
		echo "Empty Fields";
		exit();
	}
	
	require_once 'config.php';
	
	$email = $conn->real_escape_string($_POST['email']);
	$old_password = $conn->real_escape_string($_POST['old_password']);
	$new_password = $conn->real_escape_string($_POST['new_password']);
	
	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		echo "Invalid Email";
		exit();
	}
	
	$stmt = $conn->prepare("SELECT id, password FROM users WHERE email = ?");
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id, $password);
	$stmt->fetch();
	if(!($stmt->num_rows)){
		echo "Email not Registered";
		exit();
	}
	$stmt->close();
	if(!(password_verify($old_password, $password))){
		echo "Incorrect Password";
		exit();
	}
	$new_password = password_hash($new_password, PASSWORD_BCRYPT);
	$stmt = $conn->prepare("UPDATE users SET password = ? WHERE id = ?");
	$stmt->bind_param("si", $new_password, $id);
	$stmt->execute();
	if($stmt->affected_rows){
		$_SESSION['userid'] = $id;
		echo "success";
		exit();
	}else{
		echo "Error";
	}