-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2017 at 01:20 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wrekart`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `image` varchar(150) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `specs` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `image`, `name`, `price`, `specs`) VALUES
(1, 'Xolo-Era-2-8GB-Nile-SDL756379054-4-4b772.png', 'Xolo Era 2', 4999, '4GB RAM, 6GB internal memory'),
(8, 'Apple-iPhone-5C-16-GB-SDL327055343-1-2f850.jpg', 'iPhone 5C 16 GB', 15999, '2GB RAM, 16GB internal memory'),
(3, 'HKHC2.jpg', 'iPhone 6 Space Grey', 26800, '13MP primary camera, iOS 8'),
(4, 'MicromaxSDL503556483ba546_476f6-5cbe6.jpg', 'Micromax Canvas 5 E481', 7685, '13 MP Rear, 5 MP Front Camera'),
(5, 'XOLO-Era2X-3GB-16GB-Gold-SDL152838957-1-83840.jpg', 'XOLO Era2X 3GB', 6789, '5 MP Front Camera, 8 MP Rear Camera'),
(2, '41thskyt96l_1_1-e18b7.jpg', 'OPPO A37 (16GB, Gray)', 9990, '5 MP Front Camera,  8 MP Rear Camera'),
(9, 'motorola-moto-e4-plus-xt1770-original-imaeup4xazefjhzz.jpeg', 'Moto E4 Plus (Iron Gray, 32 GB) ', 9999, '13MP Rear Camera, 5MP Front Camera'),
(10, 'lenovo-k5-note-pa330010in-original-imaekyazjhfqveze.jpeg', 'Lenovo Vibe K5 Note', 9999, '13MP Rear Camera, 8MP Front Camera'),
(11, 'motorola-moto-c-plus-xt1721-original-imaeup4wxss4aaua.jpeg', 'Moto C Plus (Starry Black)', 9999, '2 GB RAM | 16 GB ROM, Expandable Upto 32 GB'),
(12, 'mi-max-2-d4-original-imaew6jfzdxmg3d6.jpeg', 'Mi Max 2 (Black, 64 GB)', 16999, 'Android 7.1.1 with MiUi 8'),
(13, 'samsung-galaxy-on-nxt-sm-g610fzdhins-original-imaet97hmqvfn5a6.jpeg', 'Samsung Galaxy On Nxt', 14900, '3 GB RAM | 64 GB ROM | Expandable Upto 256 GB'),
(14, 'lenovo-k6-power-k33a42-original-imaev3qhajgvnket.jpeg', 'Lenovo K6 Power (Grey)', 9999, '3 GB RAM | 32 GB ROM | Expandable Upto 128 GB'),
(15, 'samsung-galaxy-j3-pro-sm-j320fzdgins-original-imaeurqgnaz4puyt.jpeg', 'Samsung Galaxy J3 Pro', 7990, '2 GB RAM | 16 GB ROM | Expandable Upto 128 GB'),
(16, 'infinix-hot-4-pro-x5511-b-original-imaeweh8xfcuutv2.jpeg', 'Infinix Hot 4 Pro (Bordeaux Red)', 7499, '13MP Rear Camera | 5MP Front Camera'),
(17, 'panasonic-p55-max-eb-90s55pmxn-original-imaevq4beryceusy.jpeg', 'Panasonic P55 Max (Champagne Gold)', 8499, 'MediaTek MTK6737 Quad Core 1.25 GHz Processor');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(70) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(21, 'Parinay Singh', 'parinayks@gmail.com', '$2y$08$HF7lS81ZDeRlrX48/06wJ.vRoZZIienP4oU3YBMQ24r/YlrGeQO0e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_userid` (`user_id`),
  ADD KEY `FK_prodid` (`prod_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=363;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
