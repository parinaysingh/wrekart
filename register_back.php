<?php
	session_start();
	
	if(empty($_POST['fname']) || empty($_POST['lname']) || empty($_POST['email']) || empty($_POST['password'])){
		echo "Empty Fields";
		exit();
	}
	
	require_once 'config.php';
	
	$fname = $conn->real_escape_string($_POST['fname']);
	$lname = $conn->real_escape_string($_POST['lname']);
	$email = $conn->real_escape_string($_POST['email']);
	$password = $conn->real_escape_string($_POST['password']);
	
	$name = $fname . ' ' . $lname;
	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		echo "Invalid Email";
		exit();
	}
	$password = password_hash($password, PASSWORD_BCRYPT);
	
	$stmt = $conn->prepare("SELECT id FROM users WHERE email = ?");
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id);
	$stmt->fetch();
	if($stmt->num_rows){
		echo "Email Exists!";
		exit();
	}
	$stmt->close();
	$stmt = $conn->prepare("INSERT INTO users (name, email, password) values(?, ?, ?)");
	$stmt->bind_param("sss", $name, $email, $password);
	$stmt->execute();
	if($stmt->affected_rows){
		$_SESSION['userid'] = $stmt->insert_id;
		echo "success";
	}else{
		echo "Error";
	}