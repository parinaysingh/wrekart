<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wrekart</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/index.css">
	<link rel="icon" href="images/favicon.png" type="image/gif">
</head>
<body>
	<div class="page_container">
		<div id="header_wrapper">
		<?php 
			session_start();
			require_once 'header.php';
		?>	
		<a href="#show_target">
			<div id="btn_shop_now">Shop Now</div>
		</a>
		</div>
		<div class="content_wrapper" id="show_target">
			<div id="content">
				<?php 
					require_once 'config.php';
					$query = $conn->query("SELECT * FROM product LIMIT 6");
					if(!($query->num_rows)){
						echo "No Products Yet";
					}else{
						while($result = $query->fetch_assoc()){
					
				?>
				<div class="product_wrapper_outer">
					<div class="product_wrapper_inner">
						<div class="product_image">
							<img src="images/<?php echo $result['image']; ?>" alt="product_image">
						</div>
						<div class="product_info">
							<?php echo $result['name']; ?>
						</div>
						<div class="product_info inverted">
							<?php echo $result['specs']; ?>
						</div>
						<div class="product_sub">
							<span class = "product_price">Rs <?php echo $result['price']; ?> </span>
							<a href="add_cart.php?pid=<?php echo $result['id']; ?>" class="add_to_cart"><i class="fa fa-cart-plus" aria-hidden="true"></i> <span>Add to Cart</span></a>
						</div>
					</div>
				</div>
				<?php 
						}
					}
				?>

			</div>
			<div class="clear_both"></div>
			<div id="load_more" class=""><i class="fa fa-spinner fa-spin"></i><button>Load More</button></div>
		</div>
	</div>
    <footer>
        <span>© Wrekart 2017</span>
        <a href="#">About</a>
    </footer>

    <script src="js/index.js"></script>
	<script src="https://use.fontawesome.com/5bac47f725.js"></script>
</body>
</html>
